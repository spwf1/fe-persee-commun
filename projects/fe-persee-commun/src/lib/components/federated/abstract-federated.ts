import {loadRemoteModule} from "@angular-architects/module-federation-runtime";

export abstract class AbstractFederated {
  loadRemoteModule(remoteEntry: string, exposedModule: string) {
    return loadRemoteModule({
      remoteEntry, exposedModule, type: 'module'
    });
  }
}
