import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewContainerRef} from '@angular/core';
import {AbstractFederated} from "./abstract-federated";
import { FederatedOutputEvent, FederatedParams } from './federated.model';

@Component({
  selector: 'lib-federated',
  templateUrl: './federated.component.html'
})
export class FederatedComponent extends AbstractFederated implements OnInit {
  @ViewChild('federatedContainer', {read: ViewContainerRef, static: true})
  federatedContainer: ViewContainerRef;
  @Input() remoteEntry: string;
  @Input() exposedModule: string;
  @Input() componentName: string;
  @Input() params: FederatedParams<any>[] = [];
  @Input() outputEvents: string[] = [];
  @Output() output = new EventEmitter<FederatedOutputEvent<any>>();

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.loadRemoteModule(this.remoteEntry, this.exposedModule)
      .then(m => m[this.componentName])
      .then(comp => {
        const ref: any = this.federatedContainer.createComponent(comp);
        for (let param of this.params) {
          ref.instance[param.input] = param.value;
        }
        for (let event of this.outputEvents) {
          ref.instance[event].subscribe((value: any) => this.output.emit({event, value}));
        }
      });
  }

}
