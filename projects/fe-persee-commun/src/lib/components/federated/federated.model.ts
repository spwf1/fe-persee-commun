export interface FederatedOutputEvent<T> {
  event: string;
  value: T;
}

export interface FederatedParams<T> {
  input: string;
  value: T;
}