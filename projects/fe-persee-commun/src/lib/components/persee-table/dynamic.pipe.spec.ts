import { Injector } from '@angular/core';
import { DynamicPipe } from './dynamic.pipe';

describe('DynamicPipe', () => {
  let dynamicPipe: DynamicPipe;
  let injector: any; // Utilisation du type 'any' pour contourner l'erreur de type

  beforeEach(() => {
    injector = {
      get: jest.fn(),
    };
    dynamicPipe = new DynamicPipe(injector);
  });

  it('should return the value if pipeToken is not provided', () => {
    const value = 'Test Value';
    const transformedValue = dynamicPipe.transform(value, null, null);
    expect(transformedValue).toBe(value);
  });

  it('should use the provided pipeToken and transform the value', () => {
    const value = 'Test Value';
    const pipeMock = {
      transform: jest.fn().mockReturnValue('Transformed Value'),
    };
    const pipeArgs = ['arg1', 'arg2']; // Remplacez par les arguments réels du pipe

    injector.get.mockReturnValue(pipeMock);

    const transformedValue = dynamicPipe.transform(value, 'CustomPipe', pipeArgs);
    expect(transformedValue).toBe('Transformed Value');
    expect(injector.get).toHaveBeenCalledWith('CustomPipe');
    expect(pipeMock.transform).toHaveBeenCalledWith(value, ...pipeArgs);
  });
});
