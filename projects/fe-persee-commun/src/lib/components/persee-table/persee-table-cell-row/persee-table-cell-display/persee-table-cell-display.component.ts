import {AfterViewInit, Component, Input, PipeTransform, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'lib-persee-table-cell-display',
  templateUrl: './persee-table-cell-display.component.html'
})
export class PerseeTableCellDisplayComponent implements AfterViewInit {

  @ViewChild("componentContainer", { read: ViewContainerRef })
  container: ViewContainerRef;
  @Input()
  pipe: any;
  @Input()
  pipeArgs: any[];
  @Input()
  value: any;
  @Input()
  fieldName: keyof any;
  @Input()
  component: any;
  @Input()
  componentInputs: any[];

  ngAfterViewInit(): void {
    if (this.component) {
      const ref = this.container.createComponent(this.component, this.value);
      (ref.instance as any).value = this.value;
    }
  }
}
