import {Component, Input} from '@angular/core';
import {PerseeTableColumn} from "../persee-table.model";

@Component({
  selector: 'tr[app-table-row]',
  templateUrl: './persee-table-cell-row.component.html',
  styleUrls: ['./persee-table-cell-row.component.scss']
})
export class PerseeTableCellRowComponent {

  @Input()
  columnConfig: PerseeTableColumn<any>[];
  @Input()
  value: any;
  @Input()
  isManageColumn = true;

  selected = false;
}
