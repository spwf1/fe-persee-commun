import { TestBed } from '@angular/core/testing';

import { PerseeTableConfigStorageService } from './persee-table-config-storage.service';

describe('PerseeTableConfigStorageService', () => {
  let service: PerseeTableConfigStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PerseeTableConfigStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
