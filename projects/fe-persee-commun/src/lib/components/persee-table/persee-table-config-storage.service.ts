import {Injectable} from '@angular/core';
import {FilterParam, SortParam} from './persee-table.model';

@Injectable({
  providedIn: 'root',
})
export class PerseeTableConfigStorageService {
  readonly localStorageKey = 'persee-table';

  storeSortParam(listId: string, sortParams: SortParam<any>[]) {
    this.updateListStorage(listId, (storeItem) => (storeItem.sortParams = sortParams));
  }

  storeFilterParam(listId: string, filterPram: FilterParam<any>[]) {
    this.updateListStorage(listId, (storeItem) => (storeItem.filterParam = filterPram));
  }

  storeColumnSize(listId: string, columnSizeConfig: PerseeTableResizeStorageItem) {
    this.updateListStorage(listId, (storeItem) => {
      if (!storeItem.columnResize) {
        storeItem.columnResize = [];
      }
      const resizeItem = storeItem.columnResize.find((col) => col.colId === columnSizeConfig.colId);
      if (resizeItem) {
        resizeItem.width = columnSizeConfig.width;
      } else {
        storeItem.columnResize.push(columnSizeConfig);
      }
    });
  }

  storeColumnStateAndOrder(listId: string, columnStateList: ColumnState[]) {
    this.updateListStorage(listId, (storeItem) => (storeItem.displayAndOrder = columnStateList));
  }

  storePageIndex(listId: string, pageEvent: { pageSize: number; pageNumber: number }) {
    this.updateListStorage(listId, (storeItem) => (storeItem.page = pageEvent));
  }

  getOrAddListStorage(listId: string): PerseeTableStorageItem {
    const map = this.getOrCreateStoredMap();
    return map.get(listId) || {};
  }

  private updateListStorage(listId: string, updateFn: (storeItem: PerseeTableStorageItem) => void) {
    const map = this.getOrCreateStoredMap();
    const storeItem = map.get(listId) || {};
    updateFn(storeItem);
    map.set(listId, storeItem);
    localStorage.setItem(this.localStorageKey, JSON.stringify(Array.from(map.entries())));
  }

  private getOrCreateStoredMap(): Map<string, PerseeTableStorageItem> {
    const storageString = localStorage.getItem(this.localStorageKey);
    return storageString ? new Map(JSON.parse(storageString)) : new Map<string, PerseeTableStorageItem>();
  }
}

export interface PerseeTableStorageItem {
  sortParams?: SortParam<any>[];
  filterParam?: FilterParam<any>[];
  page?: { pageSize: number, pageNumber: number };
  paginationSize?: number;
  columnResize?: PerseeTableResizeStorageItem[];
  displayAndOrder?: ColumnState[];
}
export interface ColumnState {
  fieldName: string;
  isDisplay: boolean;
}
export interface PerseeTableResizeStorageItem {
  colId: string;
  width: number;
}

export interface PerseeTablePaginationStorageItem {
  pageIndex: number;
  pageSize: number;
}
