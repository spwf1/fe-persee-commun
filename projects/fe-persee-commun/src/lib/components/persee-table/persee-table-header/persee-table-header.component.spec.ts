import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {PerseeTableHeaderComponent} from './persee-table-header.component';
import {FilterType, PerseeSortState} from "../persee-table.model";
import {InputTextModule} from "primeng/inputtext";
import {CalendarModule} from "primeng/calendar";
import {DropdownModule} from "primeng/dropdown";

describe('PerseeTableHeaderComponent', () => {
  let component: PerseeTableHeaderComponent;
  let fixture: ComponentFixture<PerseeTableHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        InputTextModule,
        CalendarModule,
        DropdownModule
      ],
      declarations: [PerseeTableHeaderComponent],
      providers: [DatePipe]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerseeTableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit selectedSortChange event when changing the selected sort', () => {
    jest.spyOn(component.selectedSortChange, 'emit');
    const selectedSort = PerseeSortState.ASC;

    component.selectedSort = selectedSort;
    component.changeSort();

    expect(component.selectedSortChange.emit).toHaveBeenCalledWith(selectedSort);
  });

  it('should emit activeFilterChange event when changing the active filter', () => {
    jest.spyOn(component.activeFilterChange, 'emit');
    const activeFilter = 'example filter';

    component.activeFilter = activeFilter;
    component.filterChange();

    expect(component.activeFilterChange.emit).toHaveBeenCalledWith({
      isDate: false,
      value: activeFilter
    });
  });

  it('should set activeFilter as a single value and emit activeFilterChange event for non-date filter types', () => {
    jest.spyOn(component.activeFilterChange, 'emit');
    const activeFilter = 'example filter';

    component.activeFilter = activeFilter;

    expect(component.activeFilter).toEqual(activeFilter);
    expect(component.activeFilterChange.emit).not.toHaveBeenCalled();
  });

  it('should set activeFilter as a range of dates and emit activeFilterChange event for date range filter type', () => {
    jest.spyOn(component.activeFilterChange, 'emit');
    const startDate = '2023-06-01';
    const endDate = '2023-06-30';
    const activeRange = [startDate, endDate];

    component.activeFilter = activeRange;

    expect(component.activeRange).toEqual(activeRange);
    expect(component.activeFilterChange.emit).not.toHaveBeenCalledWith({
      isDate: true,
      value: activeRange
    });
  });

  it('should convert activeFilter to a date string and emit activeFilterChange event for date filter type', () => {
    jest.spyOn(component.activeFilterChange, 'emit');
    const activeDate = '2023-06-28';
    component.filterType = FilterType.DATE_PICKER;
    component.activeFilter = activeDate;

    expect(component.activeFilter).toEqual(activeDate);
    expect(component.activeFilterChange.emit).toHaveBeenCalledWith({
      isDate: true,
      value: activeDate
    });
  });
});
