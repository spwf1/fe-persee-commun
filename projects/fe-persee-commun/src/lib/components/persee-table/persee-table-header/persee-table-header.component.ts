import {Component, EventEmitter, Input, Output} from '@angular/core';
import { FilterType, PerseeSortState} from "../persee-table.model";
import {DatePipe} from "@angular/common";
import {SelectItem} from "primeng/api";
import {filter} from "rxjs";

@Component({
  selector: 'lib-persee-table-header',
  templateUrl: './persee-table-header.component.html',
  styleUrls: ['./persee-table-header.component.scss']
})
export class PerseeTableHeaderComponent {

  protected readonly sortEnum = PerseeSortState;

  @Input()
  headerTranslationKey: string;
  @Input()
  filterType: FilterType;
  @Input()
  selectedSort: PerseeSortState;
  @Input()
  fieldName: string;
  @Input()
  selectOptions: SelectItem[];
  @Input()
  isDisplay = true;
  @Output()
  selectedSortChange = new EventEmitter<PerseeSortState>();
  @Output()
  activeFilterChange = new EventEmitter<{ value: string | string[], isDate: boolean }>();
  isOpen = false;
  private _activeFilter = "";
  activeRange: string[] = [];
  protected readonly FilterType = FilterType;

  readonly sortField = [{
    value: PerseeSortState.NOT,
    labelTranslationKey: "Aucun tri",
    inputId: "unsorted"
  }, {
    value: PerseeSortState.ASC,
    labelTranslationKey: "Croissant (A - Z)",
    inputId: "ascending"
  }, {
    value: PerseeSortState.DESC,
    labelTranslationKey: "Décroissant (Z - A)",
    inputId: "descending"
  }]

  get activeFilter(): string {
    return this._activeFilter;
  }

  set activeFilter(value: string | string[]) {
    if (Array.isArray(value)) {
      this.activeRange = value;
    } else {
      this._activeFilter = value;
      if ((this.filterType === ('DATE_PICKER'))) {
        this.dateChange(this._activeFilter);
      }
    }
  }

  constructor(private datePipe: DatePipe) {}

  closeOverlay() {
    this.isOpen = false;
  }

  changeSort() {
    this.selectedSortChange.emit(this.selectedSort);
  }

  fillActiveFilter(value: string) {
    this.activeFilter = value;
  }
  dateChange($event: string) {
    this.activeFilterChange.emit({
      isDate: true,
      value: this.datePipe.transform($event, 'yyyy-MM-dd')
    });
  }

  filterChange() {
    this.activeFilterChange.emit({
      isDate: false,
      value: this._activeFilter
    });
  }

  rangeChange(event: Date[]) {
    if (event[0] && event[1]) {
      const start = this.datePipe.transform(event[0], 'yyyy-MM-dd');
      const end = this.datePipe.transform(event[1], 'yyyy-MM-dd');
      this.activeRange = [start, end];
      this.activeFilterChange.emit({
        value: [start, end], isDate: true
      });
    } else {
      this.activeRange = undefined;
      this.activeFilterChange.emit({
        value: undefined, isDate: true
      });
    }
  }

  isActive(): boolean {
    return !!((this.activeRange && this.activeRange?.length > 0) || this.activeFilter || this.selectedSort !== PerseeSortState.NOT);
  }
}
