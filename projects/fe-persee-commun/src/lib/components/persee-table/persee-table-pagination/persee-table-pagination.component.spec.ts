import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PerseeTablePaginationComponent } from './persee-table-pagination.component';

describe('PerseeTablePaginationComponent', () => {
  let component: PerseeTablePaginationComponent;
  let fixture: ComponentFixture<PerseeTablePaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PerseeTablePaginationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerseeTablePaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize with default values', () => {
    expect(component.selectedPaginationOption).toBe(10);
    expect(component.selectedPage).toBe(0);
  });

  it('should emit pagination change when page is changed', () => {
    const paginationChangeSpy = jest.spyOn(component.paginationChange, 'emit');
    component.pageChange(2);
    expect(component.selectedPage).toBe(1);
    expect(paginationChangeSpy).toHaveBeenCalledWith({ pageSize: 10, pageNumber: 1 });
  });

  it('should emit pagination change when previous page button is clicked', () => {
    const paginationChangeSpy = jest.spyOn(component.paginationChange, 'emit');
    component.goToPreviousPage();
    expect(component.selectedPage).toBe(-1);
    expect(paginationChangeSpy).toHaveBeenCalledWith({ pageSize: 10, pageNumber: -1 });
  });

  it('should emit pagination change when next page button is clicked', () => {
    const paginationChangeSpy = jest.spyOn(component.paginationChange, 'emit');
    component.goToNextPage();
    expect(component.selectedPage).toBe(1);
    expect(paginationChangeSpy).toHaveBeenCalledWith({ pageSize: 10, pageNumber: 1 });
  });

  it('should return true if it is the last page', () => {
    component.totalLength = 25;
    component.selectedPaginationOption = 10;
    component.selectedPage = 2;
    const isLastPage = component.isLastPage();
    expect(isLastPage).toBe(true);
  });

  it('should return true if it is the first page', () => {
    component.selectedPage = 0;
    const isFirstPage = component.isFirstPage();
    expect(isFirstPage).toBe(true);
  });

  it('should emit pagination change when selectedPaginationOption is changed', () => {
    const paginationChangeSpy = jest.spyOn(component.paginationChange, 'emit');
    component.selectedPaginationOption = 25;
    component.emitChange();

    expect(paginationChangeSpy).toHaveBeenCalledWith({ pageSize: 25, pageNumber: 0 });
  });

  it('should disable previous page button when it is the first page', () => {
    component.selectedPage = 0;
    fixture.detectChanges();

    const previousButtonElement = fixture.debugElement.query(By.css('#previous-button-pagination'));
    expect(previousButtonElement.nativeElement.disabled).toBe(true);
  });

  it('should disable next page button when it is the last page', () => {
    component.totalLength = 20;
    component.selectedPaginationOption = 10;
    component.selectedPage = 1;
    fixture.detectChanges();

    const nextButtonElement = fixture.debugElement.query(By.css('#next-button-pagination'));
    expect(nextButtonElement.nativeElement.disabled).toBe(true);
  });
});
