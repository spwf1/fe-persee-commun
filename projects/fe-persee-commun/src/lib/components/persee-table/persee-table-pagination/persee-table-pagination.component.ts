import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectItem} from "primeng/api";

@Component({
  selector: 'lib-persee-table-pagination',
  styleUrls: ['./persee-table-pagination.component.scss'],
  templateUrl: './persee-table-pagination.component.html'
})
export class PerseeTablePaginationComponent implements OnInit {

  @Input()
  pageSizeOptions: SelectItem[] = [
    {value: 5, label: "5"},
    {value: 10, label: "10"},
    {value: 25, label: "25"},
    {value: 50, label: "50"},
    {value: 100, label: "100"}
  ];
  @Input()
  totalLength: number;
  @Output()
  paginationChange = new EventEmitter<{pageSize: number, pageNumber: number}>();

  selectedPaginationOption: number;
  selectedPage: number;

  getTotalPage(): number {
    return Math.ceil(this.totalLength / this.selectedPaginationOption);
  }


  getDisplayedSelectedPage(): number {
    return this.selectedPage + 1;
  }

  emitChange() {
    this.paginationChange.emit({
      pageSize: this.selectedPaginationOption, pageNumber: this.selectedPage
    });
  }

  ngOnInit(): void {
    if (!this.selectedPaginationOption) {
      this.selectedPaginationOption = this.pageSizeOptions[1].value;
    }
    if (!this.selectedPage) {
      this.selectedPage = 0;
    }
  }

  pageChange($event: any) {
    const value = this.selectedPage;
    this.selectedPage = value - 1;
    this.emitChange();
  }

  goToPreviousPage() {
    this.selectedPage--;
    this.emitChange();
  }

  goToNextPage() {
    this.selectedPage++;
    this.emitChange();
  }

  isLastPage(): boolean {
    return this.getTotalPage() === (this.selectedPage + 1);
  }

  isFirstPage(): boolean {
    return this.selectedPage === 0;
  }
}
