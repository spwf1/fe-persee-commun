import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewChildren} from '@angular/core';
import {FilterParam, PerseeSortState, PerseeTableColumn, SortParam} from "./persee-table.model";
import {
  ColumnState,
  PerseeTableConfigStorageService,
  PerseeTableStorageItem
} from "./persee-table-config-storage.service";
import {PerseeTableHeaderComponent} from "./persee-table-header/persee-table-header.component";
import {PerseeTableCellRowComponent} from "./persee-table-cell-row/persee-table-cell-row.component";
import {PerseeTablePaginationComponent} from "./persee-table-pagination/persee-table-pagination.component";

@Component({
  selector: 'lib-persee-table',
  templateUrl: './persee-table.component.html',
  styleUrls: ['./persee-table.component.scss']
})
export class PerseeTableComponent implements OnInit, AfterViewInit {

  @ViewChild("paginator") paginator: PerseeTablePaginationComponent;
  @ViewChild("table") table: HTMLTableElement;
  @ViewChildren("perseeTableHeader") tableHeader: PerseeTableHeaderComponent[];
  @ViewChildren("tableRow") tableRow: PerseeTableCellRowComponent[];

  @Input() dataSource: any;
  @Input() columnConfig: PerseeTableColumn<any>[];
  @Input() maxLength: number;
  @Input() listId: string;
  @Input() selectable = true;
  @Input() isMangeColumn = true;

  @Output() sortChangeEvent = new EventEmitter<SortParam<any>[]>();
  @Output() filterChangeEvent = new EventEmitter<FilterParam<any>[]>();
  @Output() backendPaginationHandler = new EventEmitter<{ pageSize: number, pageNumber: number }>();

  sortParam: SortParam<any>[] = [];
  filterParams: FilterParam<any>[] = [];

  storageConfig: PerseeTableStorageItem;

  constructor(private perseeTableConfigStorageService: PerseeTableConfigStorageService) {
  }

  ngOnInit(): void {
    this.storageConfig = this.perseeTableConfigStorageService.getOrAddListStorage(this.listId);
    console.log(this.storageConfig);
  }

  ngAfterViewInit(): void {
    this.fillStoredParam(this.storageConfig);
    if (this.isMangeColumn) {
      this.fillDisplayAndOrder(this.storageConfig);
    }
  }

  sortChange(event: PerseeSortState, col: PerseeTableColumn<any>) {
    const selectedSortParam = this.sortParam.find(sortItem => sortItem.fieldName === col.fieldName);
    if (selectedSortParam) {
      if (event === PerseeSortState.NOT) {
        this.sortParam.splice(this.sortParam.indexOf(selectedSortParam), 1);
      } else {
        this.sortParam[this.sortParam.indexOf(selectedSortParam)].isAscending = (event === PerseeSortState.ASC);
      }
    } else {
      if (event !== PerseeSortState.NOT) {
        this.sortParam.push({
          fieldName: col.fieldName,
          isAscending: event === PerseeSortState.ASC
        });
      }
    }
    this.sortChangeEvent.emit(this.sortParam);
    this.perseeTableConfigStorageService.storeSortParam(this.listId, this.sortParam);
  }

  getActualSort(col: PerseeTableColumn<any>): PerseeSortState {
    const selectedSortParam = this.sortParam.find(sortItem => sortItem.fieldName === col.fieldName);
    return selectedSortParam ? selectedSortParam.isAscending ? PerseeSortState.ASC : PerseeSortState.DESC : PerseeSortState.NOT;
  }

  filterChange($event: { value: string | string[], isDate: boolean }, col: PerseeTableColumn<any>) {
    const selectedFilterParam = this.filterParams.find(filterItem => filterItem.fieldName === col.fieldName);
    if (!$event.value) {
      if (selectedFilterParam) {
        this.filterParams.splice(this.filterParams.indexOf(selectedFilterParam), 1);
      }
    } else {
      if (selectedFilterParam) {
        this.filterParams[this.filterParams.indexOf(selectedFilterParam)].filterValue = $event.value;
        this.filterParams[this.filterParams.indexOf(selectedFilterParam)].isDate = $event.isDate;
      } else {
        this.filterParams.push({
          fieldName: col.fieldName,
          filterValue: $event.value,
          isDate: $event.isDate
        });
      }
    }
    if (this.paginator)
      this.paginator.selectedPage = 0;
    this.paginationChange({pageSize: this.paginator.selectedPaginationOption, pageNumber: 0});
    this.filterChangeEvent.emit(this.filterParams);
    this.perseeTableConfigStorageService.storeFilterParam(this.listId, this.filterParams);

  }

  paginationChange($event: { pageSize: number; pageNumber: number }) {
    this.backendPaginationHandler.emit({
      pageNumber: $event.pageNumber, pageSize: $event.pageSize
    });
    this.perseeTableConfigStorageService.storePageIndex(this.listId, $event);
  }

  private fillStoredParam(storageConfig: PerseeTableStorageItem) {
    let isFilterOrSortChange = false;
    if (storageConfig.sortParams && storageConfig?.sortParams?.length > 0) {
      this.sortChangeEvent.emit(storageConfig.sortParams);
      this.sortParam = storageConfig.sortParams;
      isFilterOrSortChange = true;
    }
    if (storageConfig.filterParam && storageConfig?.filterParam?.length > 0) {
      this.filterChangeEvent.emit(storageConfig.filterParam);
      this.filterParams = storageConfig.filterParam;
      this.filterParams.forEach(filterParam =>{
        const header = this.tableHeader.find(header => header.fieldName === filterParam?.fieldName);
        if (header) {
          header.activeFilter = filterParam.filterValue;
        }
      });
      isFilterOrSortChange = true;
    }
    if (storageConfig.page) {
      this.paginationChange(storageConfig.page);
      this.paginator.selectedPage = storageConfig.page.pageNumber;
      this.paginator.selectedPaginationOption = storageConfig.page.pageSize;
    }
    if (!isFilterOrSortChange) {
      // apply base sort if not stored
      this.columnConfig.forEach(col => {
        if (col.baseSort !== PerseeSortState.NOT) {
          this.sortParam.push({
            fieldName: col.fieldName,
            isAscending: col.baseSort === PerseeSortState.ASC
          });
        }
      });
    }
  }

  selectOrUnSelectAll() {
    const selectedList: boolean[] = this.tableRow.map(row => row.selected);
    this.tableRow.forEach(row => row.selected = selectedList.includes(false));
  }

  getSelectedValues(): any[] {
    const selectedArray: any[] = [];
    this.tableRow.forEach(row => {
      if (row.selected) {
        selectedArray.push(row.value);
      }
    });
    return selectedArray;
  }

  setSelectedValues(values: any[]) {
    this.tableRow.forEach(row => {
      if (values.includes(row.value)) {
        row.selected = true;
      }
    });
  }

  setSelectedValuesWithFn(values: any[], testFunction: (selectedValues: any[], rowValue: any) => boolean) {
    this.tableRow.forEach(row => {
      if (testFunction(values, row.value)) {
        row.selected = true;
      }
    });
  }

  displayOrOrderChange() {
    const colsState: ColumnState[] = this.columnConfig.map(col => {
      return {
        fieldName: col.fieldName,
        isDisplay: col.isDisplay
      } as ColumnState
    });
    this.perseeTableConfigStorageService.storeColumnStateAndOrder(this.listId, colsState);
  }

  private fillDisplayAndOrder(storageConfig: PerseeTableStorageItem) {
    console.log("fill order");
    console.log(storageConfig);
    const sortedConfig: PerseeTableColumn<any>[] = [];
    storageConfig.displayAndOrder.forEach(colState => {
      const actualCol = this.columnConfig.find(col => col.fieldName === colState.fieldName);
      actualCol.isDisplay = colState.isDisplay;
      sortedConfig.push(actualCol);
    });
    console.log(sortedConfig);
    this.columnConfig = sortedConfig;
  }
}
