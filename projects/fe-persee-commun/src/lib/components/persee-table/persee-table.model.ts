import {SelectItem} from "primeng/api";

export interface PerseeTableColumn<T> {
  translationKeyHeader: string;
  fieldName: keyof T;
  pipe?: any;
  pipeArgs?: any[];
  component?: any;
  componentInputs?: {inputName: string, inputValue: any}[];
  baseSort: PerseeSortState;
  selectOptions?: SelectItem[];
  filterType?: FilterType;
  isDisplay: boolean;
}
export enum PerseeSortState {
  ASC = "ASC",
  DESC = "DESC",
  NOT = "NOT"
}
export interface SortParam<T> {
  fieldName: keyof T;
  isAscending: boolean;
}
export interface FilterParam<T> {
  fieldName: keyof T;
  filterValue: any | any[];
  isDate?: boolean;
}
export enum FilterType {
  TEXT = "TEXT",
  DATE_PICKER = "DATE_PICKER",
  INTERVAL = "INTERVAL",
  DROPDOWN = "DROPDOWN"
}
// export interface FilterSelectOption {
//   label: string;
//   value: string;
// }
