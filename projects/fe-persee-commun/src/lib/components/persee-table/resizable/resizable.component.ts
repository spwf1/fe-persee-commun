import {AfterViewInit, Component, HostBinding, Input} from '@angular/core';
import {PerseeTableConfigStorageService, PerseeTableStorageItem} from "../persee-table-config-storage.service";

@Component({
  selector: 'th[resizable]',
  templateUrl: './resizable.component.html',
  styleUrls: ['./resizable.component.scss']
})
export class ResizableComponent implements AfterViewInit {

  @Input()
  listId: string;
  @Input()
  columnId: string;

  @HostBinding("style.width.px")
  width: number | null = null;

  constructor(private perseeTableConfigStorageService: PerseeTableConfigStorageService) {
  }

  onResize(width: number) {
    this.width = width;
    this.perseeTableConfigStorageService.storeColumnSize(
      this.listId,
      {colId: this.columnId, width: width}
    );
  }

  ngAfterViewInit(): void {
    const storage: PerseeTableStorageItem = this.perseeTableConfigStorageService.getOrAddListStorage(this.listId);
    const colResizeItem = storage?.columnResize?.find(item => item.colId === this.columnId);
    if (colResizeItem) {
      this.width = colResizeItem.width;
    }
  }
}
