import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from "@angular/common";
import {PerseeDatePipe} from './pipes/persee-date.pipe';
import {PerseeNissPipe} from './pipes/persee-niss.pipe';
import {PerseeBcePipe} from './pipes/persee-bce.pipe';
import {PerseeAccountNumberPipe} from './pipes/persee-account-number.pipe';
import {FederatedComponent} from './components/federated/federated.component';
import {PerseeLastNamePipe} from './pipes/persee-last-name.pipe';
import {PerseeFirstNamePipe} from './pipes/persee-first-name.pipe';
import {PerseeAmountPipe} from './pipes/persee-amount.pipe';
import {PerseeVcsPipe} from './pipes/persee-vcs.pipe';
import {DynamicPipe} from './components/persee-table/dynamic.pipe';
import {ResizableComponent} from './components/persee-table/resizable/resizable.component';
import {ResizableDirective} from './components/persee-table/resizable/resizable.directive';
import {FormsModule} from "@angular/forms";
import {PerseeTableHeaderComponent} from "./components/persee-table/persee-table-header/persee-table-header.component";
import {PerseeTableComponent} from "./components/persee-table/persee-table.component";
import {
  PerseeTableCellDisplayComponent
} from "./components/persee-table/persee-table-cell-row/persee-table-cell-display/persee-table-cell-display.component";
import {
  PerseeTableCellRowComponent
} from "./components/persee-table/persee-table-cell-row/persee-table-cell-row.component";
import {
  PerseeTablePaginationComponent
} from "./components/persee-table/persee-table-pagination/persee-table-pagination.component";
import {OverlayModule} from "primeng/overlay";
import {RadioButtonModule} from "primeng/radiobutton";
import {CalendarModule} from "primeng/calendar";
import {DropdownModule} from "primeng/dropdown";
import {InputTextModule} from "primeng/inputtext";
import {DialogModule} from "primeng/dialog";
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import {DialogService} from "primeng/dynamicdialog";
import { PerseeTableHeaderDisplayComponent } from './components/persee-table/persee-table-header/persee-table-header-display/persee-table-header-display.component';
import { ManageColumnButtonComponent } from './components/persee-table/manage-column-button/manage-column-button.component';
import {CheckboxModule} from "primeng/checkbox";
import {OrderListModule} from "primeng/orderlist";

@NgModule({
  declarations: [
    PerseeDatePipe,
    PerseeNissPipe,
    PerseeBcePipe,
    PerseeAccountNumberPipe,
    FederatedComponent,
    PerseeLastNamePipe,
    PerseeFirstNamePipe,
    PerseeAmountPipe,
    PerseeVcsPipe,
    PerseeTableComponent,
    PerseeTableHeaderComponent,
    DynamicPipe,
    PerseeTableCellDisplayComponent,
    ResizableComponent,
    ResizableDirective,
    PerseeTableCellRowComponent,
    PerseeTablePaginationComponent,
    ConfirmationDialogComponent,
    PerseeTableHeaderDisplayComponent,
    ManageColumnButtonComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    OverlayModule,
    RadioButtonModule,
    CalendarModule,
    DropdownModule,
    InputTextModule,
    DialogModule,
    CheckboxModule,
    OrderListModule
  ],
  providers: [
    DatePipe,
    DialogService
  ],
  exports: [
    PerseeDatePipe,
    PerseeNissPipe,
    PerseeBcePipe,
    PerseeAccountNumberPipe,
    PerseeLastNamePipe,
    PerseeFirstNamePipe,
    PerseeAmountPipe,
    PerseeVcsPipe,
    FederatedComponent,
    PerseeTableComponent
  ]
})
export class FePerseeCommunModule {
}
