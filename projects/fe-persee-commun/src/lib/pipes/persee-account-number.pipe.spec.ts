import {PerseeAccountNumberPipe} from './persee-account-number.pipe';

describe('PerseeAccountNumberPipe', () => {
  it('create an instance', () => {
    const pipe = new PerseeAccountNumberPipe();
    expect(pipe).toBeTruthy();
  });


  it('should well display if valid number', () => {
    const pipe = new PerseeAccountNumberPipe();
    const actual = pipe.transform("BE38061112227972");
    expect(actual).toEqual("BE38 0611 1222 7972");
  });

  it('should well render value not formated if invalid length', () => {
    const pipe = new PerseeAccountNumberPipe();
    const actual = pipe.transform("BE380611122279722");
    const actual2 = pipe.transform("BE38061112");
    expect(actual).toEqual("BE380611122279722");
    expect(actual2).toEqual("BE38061112");
  });

  it('should not give error with null or undefined value', () => {
    const pipe = new PerseeAccountNumberPipe();
    const actualNull = pipe.transform(null);
    const actualUndefined = pipe.transform(undefined);
    expect(actualNull).toBeNull();
    expect(actualUndefined).toBeUndefined();
  });
});
