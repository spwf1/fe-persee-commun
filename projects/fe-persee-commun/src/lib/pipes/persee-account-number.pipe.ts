import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'perseeAccountNumber'
})
export class PerseeAccountNumberPipe implements PipeTransform {

  transform(value: string): string {
    if (!value || value?.length !== 16) {
      return value; // Retourne la valeur d'origine si la longueur n'est pas de 16 caractères
    }

    const country = value.substring(0, 4);
    const bankCode = value.substring(4, 8);
    const branchCode = value.substring(8, 12);
    const accountNumber = value.substring(12, 16);

    return `${country} ${bankCode} ${branchCode} ${accountNumber}`;
  }
}
