import { PerseeAmountPipe } from './persee-amount.pipe';

describe('PerseeAmountPipe', () => {
  const pipe = new PerseeAmountPipe();
  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should format the amount correctly', () => {
    const amount = 10;
    const transformed = pipe.transform(amount);
    expect(transformed).toBe('10.00 €');
  });

  it('should return empty string for non-numeric values', () => {
    const invalidAmount = 'invalid';
    const transformed = pipe.transform(invalidAmount);
    expect(transformed).toBe('');
  });

  it('should handle string values correctly', () => {
    const stringAmount = '15.5';
    const transformed = pipe.transform(stringAmount);
    expect(transformed).toBe('15.50 €');
  });

  it('should handle undefined and null values', () => {
    const undefinedAmount: string = undefined;
    const transformedUndefined = pipe.transform(undefinedAmount);
    expect(transformedUndefined).toBe('');

    const nullAmount: string = null;
    const transformedNull = pipe.transform(nullAmount);
    expect(transformedNull).toBe('');
  });
});
