import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'perseeAmount'
})
export class PerseeAmountPipe implements PipeTransform {

  transform(amount: string | number): string {
    const value = typeof amount === 'string' ? parseFloat(amount) : amount;
    if (isNaN(value) || !value) {
      return '';
    }
    return value.toFixed(2) + ' €';;
  }

}
