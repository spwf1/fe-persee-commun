import { PerseeBcePipe } from './persee-bce.pipe';

describe('PerseeBcePipe', () => {
  let pipe: PerseeBcePipe;

  beforeEach(() => {
    pipe = new PerseeBcePipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return the original value if length is not 10', () => {
    const invalidValue = '123';
    const transformed = pipe.transform(invalidValue);
    expect(transformed).toBe(invalidValue);
  });

  it('should transform the value correctly if length is 10', () => {
    const value = '1234567890';
    const transformed = pipe.transform(value);
    expect(transformed).toBe('1234.567.890');
  });

  it('should handle undefined and null values', () => {
    const undefinedValue: string = undefined;
    const transformedUndefined = pipe.transform(undefinedValue);
    expect(transformedUndefined).toBe('');

    const nullValue: string = null;
    const transformedNull = pipe.transform(nullValue);
    expect(transformedNull).toBe('');
  });
});
