import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'perseeBce'
})
export class PerseeBcePipe implements PipeTransform {

  transform(value: string): string {
    if (value?.length !== 10) {
      return value ?? "";
    }

    const firstPart = value.substring(0, 4);
    const secondPart = value.substring(4, 7);
    const thirdPart = value.substring(7, 10);

    return `${firstPart}.${secondPart}.${thirdPart}`;
  }

}
