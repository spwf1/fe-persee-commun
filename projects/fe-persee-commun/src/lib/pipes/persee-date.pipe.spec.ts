import { PerseeDatePipe } from './persee-date.pipe';
import { DatePipe } from '@angular/common';

describe('PerseeDatePipe', () => {
  let pipe: PerseeDatePipe;
  let datePipe: DatePipe;

  beforeEach(() => {
    datePipe = new DatePipe('en-US');
    pipe = new PerseeDatePipe(datePipe);
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty string if value is null or undefined', () => {
    const transformedNull = pipe.transform(null);
    expect(transformedNull).toBe('');

    const transformedUndefined = pipe.transform(undefined);
    expect(transformedUndefined).toBe('');
  });

  it('should transform the date correctly', () => {
    const date = new Date('2023-06-21');
    const transformed = pipe.transform(date);
    const expected = datePipe.transform(date, 'dd/MM/yyyy');
    expect(transformed).toBe(expected);
  });
});
