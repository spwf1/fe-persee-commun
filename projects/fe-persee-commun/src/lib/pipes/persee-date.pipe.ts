import { Pipe, PipeTransform } from '@angular/core';
import {DatePipe} from "@angular/common";

@Pipe({
  name: 'perseeDate'
})
export class PerseeDatePipe implements PipeTransform {

  constructor(private datepipe: DatePipe) {
  }
  transform(value: string | Date): string {
    if (!value) {
      return "";
    }
    return this.datepipe.transform(value, "dd/MM/yyyy");
  }

}
