import { PerseeFirstNamePipe } from './persee-first-name.pipe';

describe('PerseeFirstNamePipe', () => {
  let pipe: PerseeFirstNamePipe;

  beforeEach(() => {
    pipe = new PerseeFirstNamePipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty string if prenom is null or undefined', () => {
    const transformedNull = pipe.transform(null);
    expect(transformedNull).toBe('');

    const transformedUndefined = pipe.transform(undefined);
    expect(transformedUndefined).toBe('');
  });

  it('should transform the prenom correctly', () => {
    const prenom = 'john doe';
    const transformed = pipe.transform(prenom);
    expect(transformed).toBe('John-Doe');
  });

  it('should handle single-word prenom correctly', () => {
    const prenom = 'emma';
    const transformed = pipe.transform(prenom);
    expect(transformed).toBe('Emma');
  });

  it('should handle prenom with leading/trailing spaces correctly', () => {
    const prenom = '   john   ';
    const transformed = pipe.transform(prenom);
    expect(transformed).toBe('John');
  });

  it('should handle empty prenom correctly', () => {
    const prenom = '';
    const transformed = pipe.transform(prenom);
    expect(transformed).toBe('');
  });

  it('should handle prenom with multiple spaces correctly', () => {
    const prenom = 'mary   ann   smith';
    const transformed = pipe.transform(prenom);
    expect(transformed).toBe('Mary-Ann-Smith');
  });
});
