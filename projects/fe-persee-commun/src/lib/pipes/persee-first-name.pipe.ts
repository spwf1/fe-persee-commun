import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'perseeFirstName'
})
export class PerseeFirstNamePipe implements PipeTransform {

  transform(prenom: string): string {
    if (!prenom) {
      return '';
    }
    const prenoms = prenom.trim().split(/\s+/);
    const formattedPrenoms = prenoms.map(p => {
      const firstLetter = p.charAt(0).toUpperCase();
      const rest = p.substr(1).toLowerCase();
      return (firstLetter + rest);
    });
    return formattedPrenoms.join('-');
  }

}
