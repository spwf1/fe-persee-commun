import { PerseeLastNamePipe } from './persee-last-name.pipe';

describe('PerseeLastNamePipe', () => {
  let pipe: PerseeLastNamePipe;

  beforeEach(() => {
    pipe = new PerseeLastNamePipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty string if value is null or undefined', () => {
    const transformedNull = pipe.transform(null);
    expect(transformedNull).toBe('');

    const transformedUndefined = pipe.transform(undefined);
    expect(transformedUndefined).toBe('');
  });

  it('should transform the value to uppercase', () => {
    const value = 'doe';
    const transformed = pipe.transform(value);
    expect(transformed).toBe('DOE');
  });

  it('should handle empty value correctly', () => {
    const value = '';
    const transformed = pipe.transform(value);
    expect(transformed).toBe('');
  });

  it('should handle whitespace value correctly', () => {
    const value = '   ';
    const transformed = pipe.transform(value);
    expect(transformed).toBe('');
  });
});
