import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'perseeLastName'
})
export class PerseeLastNamePipe implements PipeTransform {

  transform(value: string): string {
    if (!value || !value?.trim()) {
      return '';
    }
    return value?.toUpperCase();
  }

}
