import { PerseeNissPipe } from './persee-niss.pipe';

describe('PerseeNissPipe', () => {
  let pipe: PerseeNissPipe;

  beforeEach(() => {
    pipe = new PerseeNissPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return the original value if length is not 11', () => {
    const value = '1234567890';
    const transformed = pipe.transform(value);
    expect(transformed).toBe(value);
  });

  it('should transform the value correctly', () => {
    const value = '12345678901';
    const transformed = pipe.transform(value);
    const expected = '12.34.56-789.01';
    expect(transformed).toBe(expected);
  });
});
