import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'perseeNiss'
})
export class PerseeNissPipe implements PipeTransform {

  transform(value: string): string {
    if (value.length !== 11) {
      return value; // Retourne la valeur d'origine si la longueur n'est pas de 11 caractères
    }

    const firstPart = value.substring(0, 2);
    const secondPart = value.substring(2, 4);
    const thirdPart = value.substring(4, 6);
    const fourthPart = value.substring(6, 9);
    const fifthPart = value.substring(9, 11);

    return `${firstPart}.${secondPart}.${thirdPart}-${fourthPart}.${fifthPart}`;
  }

}
