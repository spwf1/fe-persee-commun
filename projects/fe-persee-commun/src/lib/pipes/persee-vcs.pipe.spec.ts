import { PerseeVcsPipe } from './persee-vcs.pipe';

describe('PerseeVcsPipe', () => {
  let pipe: PerseeVcsPipe;

  beforeEach(() => {
    pipe = new PerseeVcsPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform the value correctly', () => {
    const value = 'A1B2C3456789';
    const transformed = pipe.transform(value);
    const expected = '123/4567/89';
    expect(transformed).toBe(expected);
  });

  it('should handle value with non-digit characters correctly', () => {
    const value = 'A1B2C3456D789';
    const transformed = pipe.transform(value);
    const expected = '123/4567/89';
    expect(transformed).toBe(expected);
  });

  it('should handle value with leading/trailing non-digit characters correctly', () => {
    const value = 'XYZ123/4567/89ABC';
    const transformed = pipe.transform(value);
    const expected = '123/4567/89';
    expect(transformed).toBe(expected);
  });

  it('should handle empty value correctly', () => {
    const value = '';
    const transformed = pipe.transform(value);
    expect(transformed).toBe('');
  });
});
