import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'perseeVcs'
})
export class PerseeVcsPipe implements PipeTransform {

  transform(value: string): string {
    if (!value) {
      return "";
    }
    const digitsOnly = value.replace(/\D/g, '');
    const firstGroup = digitsOnly.slice(0, 3);
    const secondGroup = digitsOnly.slice(3, 7);
    const thirdGroup = digitsOnly.slice(7, 12);
    const formattedSuite = `${firstGroup}/${secondGroup}/${thirdGroup}`;
    return formattedSuite;
  }

}
