import { FilterParam, SortParam } from '../components/persee-table/persee-table.model';
import {filterItemForParam, filterList, paginateList, sortList} from "./tableUtils";

describe('paginateList', () => {
  it('should return the paginated list', () => {
    const list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const pageNumber = 1;
    const pageSize = 3;
    const expectedPaginatedList = [4, 5, 6];

    const result = paginateList(list, pageNumber, pageSize);

    expect(result).toEqual(expectedPaginatedList);
  });
});

describe('filterList', () => {
  it('should return the filtered list', () => {
    const list = [
      { id: 1, name: 'John' },
      { id: 2, name: 'Jane' },
      { id: 3, name: 'Alice' },
      { id: 4, name: 'Bob' },
    ];
    const filterParams: FilterParam<{ id: number; name: string }>[] = [
      { fieldName: 'name', filterValue: 'J' },
    ];
    const expectedFilteredList = [
      { id: 1, name: 'John' },
      { id: 2, name: 'Jane' },
    ];

    const result = filterList(list, filterParams);

    expect(result).toEqual(expectedFilteredList);
  });
});

describe('sortList', () => {
  it('should return the sorted list', () => {
    const list = [
      { id: 1, name: 'John' },
      { id: 3, name: 'alice' },
      { id: 2, name: 'Jane' },
      { id: 4, name: 'Jene' },
    ];
    const sortParams: SortParam<{ id: number; name: string }>[] = [
      { fieldName: 'name', isAscending: true },
    ];
    const expectedSortedList = [
      { id: 3, name: 'alice' },
      { id: 2, name: 'Jane' },
      { id: 4, name: 'Jene' },
      { id: 1, name: 'John' }
    ];

    const result = sortList(list, sortParams);

    expect(result).toEqual(expectedSortedList);
  });
});




describe('filterList', () => {
  it('should return the filtered list based on filter parameters', () => {
    const completeList = [
      { id: 1, name: 'John', age: 30 },
      { id: 2, name: 'Jane', age: 25 },
      { id: 3, name: 'Alice', age: 35 },
      { id: 4, name: 'Bob', age: 40 },
    ];
    const filterParams: FilterParam<{ id: number; name: string; age: number }>[] = [
      { fieldName: 'name', filterValue: 'J' },
      { fieldName: 'age', filterValue: '3' },
    ];
    const expectedFilteredList = [
      { id: 1, name: 'John', age: 30 }
    ];

    const result = filterList(completeList, filterParams);

    expect(result).toEqual(expectedFilteredList);
  });
});

describe('filterItemForParam', () => {
  it('should filter item based on the filter parameter', () => {
    const item = { id: 1, name: 'John', age: 30 };
    const filterParam: FilterParam<typeof item> = { fieldName: 'name', filterValue: 'J' };

    const result = filterItemForParam(item, filterParam);

    expect(result).toBe(true);
  });

  it('should handle date filtering correctly', () => {
    const item = { id: 1, name: 'John', birthDate: '1990-05-15' };
    const filterParam: FilterParam<typeof item> = {
      fieldName: 'birthDate',
      filterValue: '1990-05-15',
      isDate: true,
    };

    const result = filterItemForParam(item, filterParam);

    expect(result).toBe(true);
  });

  it('should handle date range filtering correctly', () => {
    const item = { id: 1, name: 'John', birthDate: '1990-05-15' };
    const filterParam: FilterParam<typeof item> = {
      fieldName: 'birthDate',
      filterValue: ['1980-05-15', '1995-05-15'],
      isDate: true,
    };

    const result = filterItemForParam(item, filterParam);

    expect(result).toBe(true);
  });
});

