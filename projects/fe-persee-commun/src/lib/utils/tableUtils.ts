import {FilterParam, SortParam} from "../components/persee-table/persee-table.model";
import {convertToDate} from "./utils";

export function paginateList(filteredSortedLIst: any[], pageNumber: number, pageSize: number): any[] {
  return filteredSortedLIst.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize)
}

export function sortList<T>(notPaginatedList: T[], sortParams: SortParam<T>[]): T[] {
  sortParams.forEach(sort =>
    notPaginatedList = sortByProperty<T>(notPaginatedList, sort.fieldName, sort.isAscending));
  return notPaginatedList;
}

export function filterList<T>(completeList: T[], filterParams: FilterParam<T>[]): T[] {
  let filteredList: T[] = [...completeList];
  filterParams.forEach(filter =>
    filteredList = filteredList.filter(item => filterItemForParam<T>(item, filter)));
  return filteredList;
}

export function filterItemForParam<T>(item: T, filterItem: FilterParam<T>): boolean {
  const value = item[filterItem.fieldName];
  if (filterItem.isDate) {
    if (Array.isArray(filterItem.filterValue)) {
      const rangeStart = convertToDate(filterItem.filterValue[0]).getTime();
      const rangeEnd = convertToDate(filterItem.filterValue[1]).getTime();
      const dateValue = convertToDate(item[filterItem.fieldName] as string).getTime();
      return dateValue > rangeStart && dateValue < rangeEnd;
    } else {
      const dateValue = convertToDate(item[filterItem.fieldName] as string);
      const filterDate = convertToDate(filterItem.filterValue);
      return dateValue.getTime() === filterDate.getTime();
    }
  } else if (typeof value === 'string') {
    return (value as string).includes(filterItem.filterValue);
  } else if (typeof value === 'number') {
    return value.toString().includes(filterItem.filterValue);
  }
  return undefined;
}

export function sortByProperty<T>(array: T[], property: keyof T, ascending: boolean = true): T[] {
  return array.sort((a, b) => {
    let valueA = a[property];
    let valueB = b[property];

    if (valueA === valueB) {
      return 0;
    }

    if (typeof valueA === "string") {
      (valueA as any) = valueA.toLowerCase();
    }

    if (typeof valueB === "string") {
      (valueB as any) = valueB.toLowerCase();
    }

    if (ascending) {
      return valueA > valueB ? 1 : -1;
    } else {
      return valueA < valueB ? 1 : -1;
    }
  });
}
