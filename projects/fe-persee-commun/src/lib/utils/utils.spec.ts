import {aRandomDate, aRandomNumber, aRandomString, convertToDate} from "./utils";

describe('aRandomString', () => {
  test('returns a random string of specified length', () => {
    const length = 10;
    const randomString = aRandomString(length);
    expect(randomString.length).toBe(length);
    expect(typeof randomString).toBe('string');
  });
});

describe('aRandomDate', () => {
  test('returns a random date between the specified start and end dates', () => {
    const startDate = new Date('2022-01-01');
    const endDate = new Date('2022-12-31');
    const randomDate = aRandomDate(startDate, endDate);
    expect(randomDate.getTime()).toBeGreaterThanOrEqual(startDate.getTime());
    expect(randomDate.getTime()).toBeLessThanOrEqual(endDate.getTime());
  });
});

describe('aRandomNumber', () => {
  test('returns a random number between the specified min and max values', () => {
    const min = 1;
    const max = 10;
    const randomNumber = aRandomNumber(min, max);
    expect(randomNumber).toBeGreaterThanOrEqual(min);
    expect(randomNumber).toBeLessThanOrEqual(max);
  });
});

describe('convertToDate', () => {
  test('converts a "dd/MM/yyyy" string to a valid Date object', () => {
    const dateString = '01/01/2022';
    const expectedDate = new Date(2022, 0, 1);
    const convertedDate = convertToDate(dateString);
    expect(convertedDate.getTime()).toBe(expectedDate.getTime());
  });
});
