export function aRandomString(length = 10): string {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let randomString = '';

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    randomString += characters.charAt(randomIndex);
  }

  return randomString;
}

export function aRandomDate(startDate?: Date, endDate?: Date): Date {
  const today = new Date();
  const defaultEndDate = new Date(today.getTime() + 7 * 24 * 60 * 60 * 1000); // Date du jour + 1 semaine en millisecondes

  const startTimestamp = startDate ? startDate.getTime() : today.getTime();
  const endTimestamp = endDate ? endDate.getTime() : defaultEndDate.getTime();

  const randomTimestamp = startTimestamp + Math.random() * (endTimestamp - startTimestamp);
  return new Date(randomTimestamp);
}

export function aRandomNumber(min = 0, max = 100): number {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function convertToDate(dateString: string) {
  //  Convert a "dd/MM/yyyy" string into a Date object
  let d = dateString.split("/");
  let dat = new Date(d[2] + '/' + d[1] + '/' + d[0]);
  return dat;
}
