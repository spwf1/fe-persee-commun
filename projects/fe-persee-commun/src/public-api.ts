/*
 * Public API Surface of fe-persee-commun
 */

export * from './lib/fe-persee-commun.module';
export * from './lib/pipes/persee-date.pipe';
export * from './lib/pipes/persee-niss.pipe';
export * from './lib/pipes/persee-bce.pipe';
export * from './lib/pipes/persee-account-number.pipe';
export * from './lib/pipes/persee-last-name.pipe';
export * from './lib/pipes/persee-first-name.pipe';
export * from './lib/pipes/persee-amount.pipe';
export * from './lib/pipes/persee-vcs.pipe';
export * from './lib/services/shared.service';
export * from './lib/components/federated/federated.component';
export * from './lib/components/federated/abstract-federated';
export * from './lib/components/persee-table/persee-table.component';
export * from './lib/components/federated/federated.model';
export * from './lib/services/transversal-sub-menu.service';
export * from './lib/models/federated-tab-menu.model';
export * from './lib/abstract/abstract-subscription-container';
