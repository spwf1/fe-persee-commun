import {Component} from '@angular/core';
import {
  PerseeTableConfigStorageService
} from "../../../fe-persee-commun/src/lib/components/persee-table/persee-table-config-storage.service";
import {
  AbstractSubscriptionContainer
} from "../../../fe-persee-commun/src/lib/abstract/abstract-subscription-container";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']

})
export class AppComponent extends AbstractSubscriptionContainer {
  title = 'test-application';

  constructor() {
    super();
  }
}

export interface testModel {
  value: string;
}
