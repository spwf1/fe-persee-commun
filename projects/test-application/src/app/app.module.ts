import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FePerseeCommunModule} from "../../../fe-persee-commun/src/lib/fe-persee-commun.module";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {ListContainerComponent} from './list-container/list-container.component';
import {TestDisplayComponent} from './list-container/test-display/test-display.component';
import {FormsModule} from "@angular/forms";
import {CollapsableTestComponent} from './list-container/collapsable-test/collapsable-test.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { TestDialogComponent } from './dialog-container/test-dialog/test-dialog.component';
import {TabViewModule} from "primeng/tabview";
import { DialogContainerComponent } from './dialog-container/dialog-container.component';
import { PipeContainerComponent } from './pipe-container/pipe-container.component';
import { UtilsContainerComponent } from './utils-container/utils-container.component';
import {PanelModule} from "primeng/panel";

@NgModule({
  declarations: [
    AppComponent,
    ListContainerComponent,
    TestDisplayComponent,
    CollapsableTestComponent,
    TestDialogComponent,
    DialogContainerComponent,
    PipeContainerComponent,
    UtilsContainerComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FePerseeCommunModule,
    FormsModule,
    TabViewModule,
    PanelModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
