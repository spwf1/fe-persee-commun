import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollapsableTestComponent } from './collapsable-test.component';

describe('CollapsableTestComponent', () => {
  let component: CollapsableTestComponent;
  let fixture: ComponentFixture<CollapsableTestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CollapsableTestComponent]
    });
    fixture = TestBed.createComponent(CollapsableTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
