import { Component } from '@angular/core';

@Component({
  selector: 'app-collapsable-test',
  templateUrl: './collapsable-test.component.html',
  styleUrls: ['./collapsable-test.component.scss']
})
export class CollapsableTestComponent {

  isOpen = false;
  collapseTest() {
    this.isOpen = !this.isOpen;
  }
}
