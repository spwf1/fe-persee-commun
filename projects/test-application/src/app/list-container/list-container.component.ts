import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {
  FilterParam,
  FilterType,
  PerseeSortState,
  PerseeTableColumn,
  SortParam
} from "../../../../fe-persee-commun/src/lib/components/persee-table/persee-table.model";
import {aRandomNumber, aRandomString} from "../../../../fe-persee-commun/src/lib/utils/utils";
import {PerseeNissPipe} from "../../../../fe-persee-commun/src/lib/pipes/persee-niss.pipe";
import {DatePipe} from "@angular/common";
import {PerseeAmountPipe} from "../../../../fe-persee-commun/src/lib/pipes/persee-amount.pipe";
import {
  PerseeTableComponent
} from "../../../../fe-persee-commun/src/lib/components/persee-table/persee-table.component";
import {filterList, paginateList, sortList} from "../../../../fe-persee-commun/src/lib/utils/tableUtils";
import {CollapsableTestComponent} from "./collapsable-test/collapsable-test.component";

@Component({
  selector: 'app-list-container',
  templateUrl: './list-container.component.html',
  styleUrls: ['./list-container.component.scss'],
  providers: [PerseeNissPipe, PerseeAmountPipe]
})
export class ListContainerComponent implements OnInit, AfterViewInit {

  @ViewChild("table") table: PerseeTableComponent;

  data: TestModel[] = [{
    startDate: "2023-06-22",
    amount: aRandomNumber(0, 100),
    description: "test description",
    niss: "96081914992",
    enterpriseName: "test"
  }, {
    startDate: "2023-06-22",
    amount: aRandomNumber(0, 100),
    description: "test description",
    niss: "96081914992",
    enterpriseName: "test"
  }, {
    startDate: "2023-06-22",
    amount: 68,
    description: "test description",
    niss: "96081914992",
    enterpriseName: "test"
  }];

  filteredData: TestModel[] = [];

  column: PerseeTableColumn<TestModel>[] = [{
    fieldName: "startDate",
    translationKeyHeader: "Start date",
    baseSort: PerseeSortState.NOT,
    filterType: FilterType.DATE_PICKER,
    pipe: DatePipe,
    pipeArgs: ['dd/MM/yyyy'],
    isDisplay: false
  }, {
    fieldName: "amount",
    translationKeyHeader: "Amount",
    baseSort: PerseeSortState.NOT,
    filterType: FilterType.DROPDOWN,
    pipe: PerseeAmountPipe,
    selectOptions: [
      {
        value: null, label: 'no filter'
      },
      {
        value: "50", label: "50"
      }, {
        value: "30", label: "30"
      }, {
        value: "20", label: "20"
      }],
    isDisplay: true
  }, {
    fieldName: "description",
    translationKeyHeader: "Description",
    baseSort: PerseeSortState.ASC,
    filterType: FilterType.TEXT,
    isDisplay: true
  }, {
    fieldName: "niss",
    translationKeyHeader: "Niss",
    baseSort: PerseeSortState.NOT,
    pipe: PerseeNissPipe,
    isDisplay: true
  }, {
    fieldName: "enterpriseName",
    translationKeyHeader: "Component test",
    baseSort: PerseeSortState.NOT,
    component: CollapsableTestComponent,
    isDisplay: true
  }];

  paginatedData: TestModel[];

  pageSize = 5;
  pageIndex = 0;

  ngOnInit(): void {
    for (let i = 0; i < 10; i++) {
      this.data.push({
        startDate: '2023-06-22',
        niss: aRandomString(11),
        description: aRandomString(35),
        amount: aRandomNumber(0, 100),
        enterpriseName: aRandomString(20)
      });
    }
    this.filteredData = [...this.data];
    this.paginatedData = [...this.data];
    this.paginationChange({pageSize: this.pageSize, pageNumber: this.pageIndex});
  }

  ngAfterViewInit() {
    this.table.setSelectedValuesWithFn([{
      startDate: "2023-06-22",
      amount: 68,
      description: "test description",
      niss: "96081914992",
      enterpriseName: "test"
    }], (selectedValues, arg2) => {
      return selectedValues.map(value => value.amount).includes(arg2.amount)
    });
  }

  sortChange($event: SortParam<TestModel>[]) {
    this.filteredData = sortList<TestModel>(this.filteredData, $event);
    this.paginationChange({pageSize: this.pageSize, pageNumber: this.pageIndex});
  }

  filterChange($event: FilterParam<TestModel>[]) {
    this.filteredData = filterList<TestModel>(this.data, $event);
    this.paginationChange({pageSize: this.pageSize, pageNumber: this.pageIndex});
  }

  paginationChange($event: { pageSize: number; pageNumber: number }) {
    this.pageIndex = $event.pageNumber;
    this.pageSize = $event.pageSize;
    this.paginatedData = paginateList(this.filteredData, this.pageIndex, this.pageSize);
  }
}

export interface TestModel {
  startDate: string;
  amount: number;
  description: string;
  niss: string;
  enterpriseName: string;
}
