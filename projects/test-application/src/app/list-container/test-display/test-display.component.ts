import {Component, Input} from '@angular/core';
import {TestModel} from "../list-container.component";

@Component({
  selector: 'app-test-display',
  templateUrl: './test-display.component.html',
  styleUrls: ['./test-display.component.scss']
})
export class TestDisplayComponent {

  @Input()
  value: TestModel;

  computeColor() {
    if (this.value.amount < 50) {
      return 'red';
    } else {
      return 'blue';
    }
  }
}
